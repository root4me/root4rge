
import sys
import os
import argparse

class Gcode:
    """ G-code parser"""

    def __init__(self,file):
        self.file = file
        self.extr_temp = 0
        self.extr_temp_at_z={}
        self.retr_speed = 0
        self.retr_at_z = {}  # Retraction and speed setting after z height is reached
        self.travel_speed = 0

        self.debug = False
        self.debug_log_lines = [0,0] # Debug info will be printed for gcode between these lines
        self.debug_exit_after = 0 # Exit processing after this line is processed while in debug mode

        self.code=[]
        self.audit=[]
        self.layers={}

        self._found_g28=False # If not found, need to insert one during post processing
        self._found_temp_before_g28=False   # If temperature setting is not found before g28, need to insert M109 before G28
        self._curr_layer=0
        self._base_curr_line=0

        self._extr_reset=False
        self._curr_retr=0.00000
        self._retr_offset=0.0
        self._prev_retr_offset=0.0

        self._base_retr=0.0 # Retraction in gcode file
        self._base_prev_extr=0.0 # Extrusion from previous line in gcode file
        self._mod_prev_extr=0.0 # Extrusion after applying offset
        self._retr_changed=False
        self._offset_changed=False

    def transform(self):
        tokens=[]
        with open(self.file,"r") as f:
            for line in f.readlines():
                self._base_curr_line+=1
                self.code.append(line)
                tokens=line.strip().split()
                if len(tokens)==0 or tokens[0][0:1]==';':
                    continue # No processing for empty lines and lined with only comments

                # Set extruder temperature
                if (tokens[0]=='M104' or tokens[0]=='M109') and self.extr_temp==0:
                    if 0.0 in self.extr_temp_at_z:
                        self.extr_temp = self.extr_temp_at_z[0.0]

                if (tokens[0]=='M104' or tokens[0]=='M109') and self.extr_temp>0:
                    curr_temp=int(tokens[1][1:])
                    if curr_temp>0: # Eliminate updating shut off procedure at end of print
                        if self._found_g28==False:  # Set temperature and wait before homing
                            tokens[0]='M109'
                            self._found_temp_before_g28=True

                        tokens[1]='S' + str(self.extr_temp)
                        self._replace_line(" ".join(tokens))
                    continue

                # Confirm G28 is found. If not found, need to insert during post processing
                if tokens[0]=='G28':
                    self.audit.append([line,"Homing found at " + str(len(self.code))])
                    self._found_g28=True
                    continue

                # Layer change
                # G1 Z0.400 F7800.000
                if len(tokens)>2 and tokens[1][0:1] == 'Z':
                    layer=float(tokens[1][1:])
                    if self._curr_layer == 0 and layer > 1:
                        continue # Discard because this will be the nozzle lift before print start

                    self.layers[layer] = len(self.code) - 1
                    self._curr_layer=layer

                    # Change temperature at layer if this layer is in the temperature list
                    if layer in self.extr_temp_at_z:
                        self._add_line('M104 S' + str(self.extr_temp_at_z[layer]))
                    continue

                # Retraction  
                # G1 X91.474 Y99.099 E40.91446
                # G1 Z1.000 F7800.000
                # G1 E38.91446 F2400.00000
                # G92 E0
                # G1 X108.289 Y101.139 F7800.000
                # G1 E2.00000 F2400.00000 

                # First retraction after offset change, adjust retraction
                # All subsequent extrusions except for retraction adjust extrusion

                if tokens[0]=='G92' and tokens[1]=='E0':
                    self._base_prev_extr=0.0
                
                if tokens[0]=='G1' and len(self.retr_at_z) > 0:
                    desired_retr=[]

                    # self.print("Retraction at z" ,self.retr_at_z)
                    for l in self.retr_at_z:
                        if self._curr_layer >= l:
                            desired_retr=self.retr_at_z[l]

                    for i in range(len(tokens)):
                        if tokens[i][0:1]=='E':
                            extr=float(tokens[i][1:])
                            new_extr=extr

                            # First extrusion we encounter will be the retraction set by slicer
                            if self._base_retr==0:
                                self._base_retr = round(self._base_prev_extr - extr,5)
                                self.print("Base retraction", str(self._base_retr))

                            self.print("Desired retraction", str(desired_retr))
                            self._retr_offset = desired_retr[0] - self._base_retr
                            if self._prev_retr_offset != self._retr_offset:
                                self._offset_changed=True

                            self.print("Previous offset",self._prev_retr_offset)
                            self.print("Current offset",self._retr_offset)
                            self.print("Previous Extrusion",self._base_prev_extr)
                            self.print("Extrusion",extr)
                            self.print("Offset Changed",self._offset_changed )
                            self.print("Retr Changed",self._retr_changed )

                            # If this is first retraction after offset change, adjust the retraction
                            if self._offset_changed and (extr < self._base_prev_extr):
                                new_extr = round(self._mod_prev_extr - desired_retr[0],5)
                                self._retr_changed=True
                                # New offset has been applied to retraction. No subsequent retraction need to be adjusted till another offset change is requested
                                self._offset_changed=False 
                                self._prev_retr_offset=self._retr_offset
                                self.print("Offset changed + Extrusion changed","")
                            elif (self._retr_changed==True and self._offset_changed==False) and (not extr < self._base_prev_extr):
                                # Keep applying the offset to all extrusions till a new offset is requested and applied to retraction
                                new_extr = round(extr + self._retr_offset,5)
                                self.print("New Extrusion :",  new_extr)
                            elif (self._retr_changed==True and self._offset_changed==True) and (not extr < self._base_prev_extr):
                                # If offset change has been requested, but we have not hit a retraction yet; keep applying prev offset
                                new_extr = round(extr + self._prev_retr_offset,5)
                                self.print("New Extrusion" , new_extr)

                            if extr != new_extr:
                                tokens[i] =  'E' + str(new_extr)
                                self._update_line(" ".join(tokens))

                            self.print("-----------------","")

                            self._base_prev_extr = extr
                            self._mod_prev_extr=new_extr
                        
                        elif tokens[i][0:1]=='F' and ('E' in line):
                            extr_speed = round(float(tokens[i][1:]),5)
                            new_extr_speed = round(float(desired_retr[1]),5)
                            self.print("Extrusion Speed",extr_speed)
                            self.print("New Extrusion Speed",new_extr_speed)
                            if extr_speed != new_extr_speed:
                                tokens[i] =  'F' + str(new_extr_speed)
                                self._update_line(" ".join(tokens))

                        elif tokens[i][0:1]=='F' and ('X' in line) and ('E' not in line):
                            self.print("Travel line",line )
                            tr_speed = round(float(tokens[i][1:]),5)
                            new_tr_speed = round(self.travel_speed,5) if self.travel_speed > 0 else tr_speed
                            self.print("Travel speed   ",tr_speed)
                            self.print("New Travel speed",new_tr_speed)
                            if tr_speed != new_tr_speed:
                                tokens[i] =  'F' + str(new_tr_speed)
                                self._update_line(" ".join(tokens))


                if self.debug==True and self._base_curr_line==self.debug_exit_after:
                    return


    def _replace_line(self,new_line):
        if not new_line.endswith('\n'):
            new_line+='\n'
        self.audit.append([len(self.code),self.code[len(self.code)-1], new_line])
        self.code[len(self.code) -1]=new_line

    def _add_line(self,new_line):
        new_line+=" ; A \n"
        self.code.append(new_line)
        self.audit.append([len(self.code),new_line, "Add"])

    def _update_line(self,new_line):
        new_line+=" ; U \n"
        self.code[len(self.code) -1]=new_line

    def print(self,label, value):
        if self.debug:
            if self._base_curr_line >= self.debug_log_lines[0] and self._base_curr_line <= self.debug_log_lines[1]:
                print("%s %s\t%s" %(self._base_curr_line,label,value))

    def apply_sensible_defaults(self):
        pass

    def write_to_file(self):
        with open("out.gcode", "w") as f:
            f.writelines(self.code)


def main_test():

    file = "Retraction.gcode"
    # Set extruder temperture to this value. Anywhere in gcode file where there is a M104 or M109, replace with this
    # temperature.
    # If there is no M109 before homing command (G28), make sure there is a M109
    extr_temp = 225
    # Set temperature at specifc layers. Later 5 - 10 at 230. Layer 10 up at 235
    extr_tempr_layers = {5.0: 230, 10.0: 235, 15.0: 240}
    # Set retraction to 2.0 . Gcode need to be generated with a retraction setting higher than 0.
    # This script merely change retraction setting. It doesn't introduce retraction if it doesnt exist
    retr = 3.0
    # Set retraction after layer 5 to 2.0 at 40mm/s . Set retraction after layer 10 to 3.0 at 40mm/s
    retr_layers = {0: [2.0 , 1200], 5.0: [2.0 , 1800] ,10.0: [2.0 , 2400],15.0: [3.0,1200], 20.0: [3.0,1800], 25.0: [3.0,2400]}
    # set travel speed to 130mm/s
    travel = 7800

    g = Gcode(file)
    g.extr_temp=extr_temp
    # g.extr_tempr_layers=extr_tempr_layers
    # g.retr=retr
    g.retr_at_z=retr_layers

    g.debug = False
    g.debug_log_lines = [90,100]
    g.debug_exit_after = 100

    g.transform()
    print(g.audit)
    g.write_to_file()
    print(g.layers)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file",help="G-code file to parse")
    parser.add_argument("-t", dest="t" , help="Extruder temperature" , type=int, default=0)
    parser.add_argument("-Z", dest="Z", default=[], type=float, nargs="+", help="List of z height in mm at which to apply temperature or retraction. eg: -Z 0 1.0 5.0 10.0")
    parser.add_argument("-T", dest="T", default=[], type=int, nargs="+", help="List of temperatures to apply eg: -T 225 230 235 240")
    parser.add_argument("-R", dest="R", default=[], type=float, nargs="+", help="List of retraction values to apply eg: -R 2.0 2.4 2.8 3.0")
    parser.add_argument("-S", dest="S", default=[], type=float, nargs="+", help="List of retraction speed to apply in mm/s eg: -S 10 20 30 40")
    parser.add_argument("-s", dest="s", default=0, type=float, help="Travel speed in mm/s eg: -s 130")
    args = parser.parse_args()

    print("File %s" % args.file)
    print("Temp %s" % args.t)
    print("Z list %s" % args.Z)
    print("Temp list %s" % args.T)
    print("Retraction list %s" % args.R)
    print("Retraction speed %s" % args.S)
    print("Travel speed %s" % args.s)

    temps = dict(zip(args.Z, args.T))
    print(temps)
    if args.t==0 and 0.0 not in temps:
        print("Temperature is required. Either provide -t or provide 0 layer temperature using -Z and -T")
        parser.print_help()
        return

    if args.t==0 and 0.0 not in args.Z:
        print("Z argument must have 0 layer. eg: -Z 0 5 10")
        parser.print_help()
        return

    # convert retraction speed mm/s to mm/min
    args.S = [round(args.S[i]*60,5) for i in range(len(args.S))]

    retraction = zip(args.R,args.S)
    z_retraction = dict(zip(args.Z,retraction))
    print(z_retraction)
    
    g = Gcode(args.file)
    g.extr_temp=args.t
    g.extr_temp_at_z=temps
    g.retr_at_z = z_retraction
    g.travel_speed = float(args.s)*60

    g.debug = False
    g.debug_log_lines = [25, 27]
    g.debug_exit_after = 28


    g.transform()
    g.write_to_file()


if __name__ == '__main__':
    # main_test()
    main()


