
import urwid

class PrinterConsoleDisplay(object):

    palette = [
        ('input expr', 'black,bold', 'light gray'),
        ('bigtext', 'white', 'black'),
    ]

    def __init__(self):
        self.view = None
        self.last_result = None
        self.last_expression = None

    def _create_view(self):

        self.input_expr = urwid.Edit(('input expr', "Command > "))


#       sb = urwid.BigText("root4rge", self._get_font_instance())
#       sb = urwid.Padding(sb, 'center', None)
#       sb = urwid.AttrWrap(sb, 'bigtext')
#       sb = urwid.Filler(sb, 'top', None, 6)
#       self.status_bar = urwid.BoxAdapter(sb, 6)

        div = urwid.Divider()
        self.header = urwid.Pile(
            [div,
             urwid.AttrMap(self.input_expr, 'input expr'), div],
            focus_item=1)
        urwid.connect_signal(self.input_expr, 'change', self._on_edit)

        # Left content
        self.printer_response = urwid.Text("")
        self.printer_response_list = [div, self.printer_response]
        self.left_content = urwid.ListBox(self.printer_response_list)
        self.left_content = urwid.LineBox(self.left_content, title='Printer Response')

        # Right content
        self.gcode_ref = urwid.Text("")
        self.gcode_ref_list = [div, self.gcode_ref]
        self.right_content = urwid.ListBox(self.gcode_ref_list)
        self.right_content = urwid.LineBox(self.right_content, title='G-code command reference')

        # Columns
        self.content = urwid.Columns([self.left_content, self.right_content])

        self.footer = urwid.Text("Status: ")

        self.view = urwid.Frame(body=self.content, header=self.header, footer=self.footer, focus_part='header')

    def _get_font_instance(self):
        return urwid.get_all_fonts()[-2][1]()

    def _on_edit(self, widget, text):
        self.last_expression = text
        self.footer.set_text(text)

    def main(self, screen=None):
        self._create_view()
        self.loop = urwid.MainLoop(self.view, self.palette, unhandled_input=self.unhandled_input, screen=screen)
        self.loop.screen.set_terminal_properties(colors=256)
        self.loop.run()


    def unhandled_input(self, key):
        if key == 'f2':
            raise urwid.ExitMainLoop()


def main():
    screen = urwid.raw_display.Screen()
    display = PrinterConsoleDisplay()
    display.main(screen=screen)

if __name__ == '__main__':
    main()
