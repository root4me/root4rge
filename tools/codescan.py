
import sys
import argparse

class GcodeParser:
    """ G-code parser """

    def __init__(self,filename):
        self.file=filename
        self.home_pos=0 
        self.extr_temp_layer1=0
        self.extr_temp_layer1_pos=0
        self.extr_temp=0
        self.extr_temp_pos=[]
        self.layers={}          # Map layer change to line number of gcode
        self.retraction=0
        self.retrations=[]      # Line numbers where retractions occur
        self.code=[]

        self._curr_layer=0
        self._curr_extrusion=0

        self._parse()
        self._post_process()
    
    def _parse(self):
        index=0
        tokens=[]
        with open(self.file,"r") as f:
            for line in f.readlines():
                self.code.append(line)
                tokens=line.strip().split()
                if len(tokens)==0:
                    index+=1
                    continue
                
                self._parse_home_pos(tokens)
                self._parse_extr_temp(tokens)
                self._parse_layer_change(tokens)
                self._parse_retraction(tokens)

                index+=1
                
    def _post_process(self):
        self.ensure_temp_before_home()

    def _parse_home_pos(self,tokens):
        if self.home_pos==0 and tokens[0]=='G28':
            self.home_pos=len(self.code) - 1

    def _parse_extr_temp(self,tokens):
        if tokens[0] != 'M104' and tokens[0] != 'M109':
            return
        
        if self.extr_temp_layer1 == 0:
            self.extr_temp_layer1 = float(tokens[1][1:])
            self.extr_temp_layer1_pos=len(self.code) - 1
        elif self.extr_temp== 0:
            self.extr_temp=float(tokens[1][1:])
            
        self.extr_temp_pos.append(len(self.code) - 1)


    def _parse_layer_change(self,tokens):
        if len(tokens) <2:
            return

        if tokens[1][0:1] == 'Z':
            layer=float(tokens[1][1:])
            if self._curr_layer == 0 and layer > 1:
                return # Discard because this will be the nozzle lift before print start

            self.layers[layer] = len(self.code) - 1
            self._curr_layer=layer
    
    def _parse_retraction(self,tokens):

        if tokens[0] != 'G92' and tokens[0] != 'G1':
            return

        for token in tokens:
            if token[0:1] == 'E':
                if float(token[1:]) < self._curr_extrusion and tokens[0] == 'G1':
                    print("Retraction @ %s %s" %(str(len(self.code) - 1), round(self._curr_extrusion - float(token[1:]),1) ))
                    if self.retraction==0:
                        self.retraction=round(self._curr_extrusion - float(token[1:]),1)
                    self.retrations.append(len(self.code) - 1)

                self._curr_extrusion=float(token[1:])

        """
        Change retraction from a layer forward:
        Find current retrtaction value. Difference between current value an new value is the offset
        curr=2 , new =3 , offset =1
        Find the first retraction after the layer number
        reduce offset from the E value. (if retraction was 2, now it becomes 3)
        There will be a G92 E0 right afer retraction
        for all subsequent E values; add offset to it except for any retraction line

        If retraction need to be changed after a subsequent layer:
        First retraction after the layer, use the new offest
        """
    
    def _offset_layer_change(self,layer,offset):
        """ 
        All layer change lines after this 'layer' will be incremented by 'offset' number of lines
        eg: If you add one line for setting temperature (M104 S240) after layer 10; call _offset_layer_change(10,1)
        """
        for key in self.layers:
            if key>layer:
                self.layers[key]+=offset

    def write_to_file(self):
        with open("out.gcode", "w") as f:
            f.writelines(self.code)
        print("-> Generated out.gcode")

    def ensure_temp_before_home(self):
        if (self.extr_temp_layer1_pos < self.home_pos):
            self.code[self.extr_temp_layer1_pos] = (self.code[self.extr_temp_layer1_pos]).replace('M104','M109')

    def apply_temp(self,temp):
        for t in self.extr_temp_pos:
            tokens = self.code[t].strip().split()
            if int(tokens[1][1:]) > 0:
                tokens[1] = 'S' + str(temp)
                self.code[t] = ' '.join(tokens) + '\n'

    def apply_temp_layers(self, temps):
        for key in temps:
            l = self.layers[key]
            self.code.insert(l,'M104 S' + str(temps[key]) + '\n')
            self._offset_layer_change(key,1)

def print_output(label,value):
    print("%s : %s"  %(label, value))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file",help="G-code file to parse")
    parser.add_argument("-t", dest="temperature" , help="temperature" , type=int, default=0)
    parser.add_argument('-T', action='append', dest='temperatures', default=[], help="List of temperatures", type=int)
    parser.add_argument('-L', action='append', dest='layers', default=[], help="List of layers. -L 5 -T 225 -L 10 -T 230", type=float)
    args = parser.parse_args()

    g=GcodeParser(sys.argv[1])

    if args.temperature > 0:
        g.apply_temp(args.temperature)

    t = dict(zip(args.layers, args.temperatures))
    g.apply_temp_layers(t)

    print_output("File" , g.file)
    print_output("G28 position" , g.home_pos)
    print_output("Retraction",g.retraction)
    print_output("Retraction lines" , g.retrations)
    #print(g.extr_temp_layer1)
    #print(g.extr_temp)

    #print(g.layers)
    #print(g.extr_temp_pos)

    #g.write_to_file()

if __name__ == '__main__':
    main()

