
# Calibration  

## Pre-req
```
usermod -aG uucp $USER    # Add user to uucp group to get permission to access /dev/ttyACM*  
pip install pyserial      # Install pyserial to use miniterm
python -m miniterm -e /dev/ttyACM0 250000
Or
python miniterm -e /dev/ttyACM0 250000  # If using local copy of miniterm
```

## Spot check motion direction

Manually move the extruder to be around the middle of the build plate (X and Y axis in middle)
```
G91           # Use relative positioning
G1 X5 F1500   # 5mm to right @ feedrate of 1500mm/min
G1 X-5 F1500  # 5mm to left @ feedrate of 1500mm/min

G1 Y5 F1500
G1 Y-5 F1500

G1 Z5 F1500
G1 Z-5 F1500

G90           # Use absolute position
```
If motion directions are not accurate check Invert direction setting in firmware  

## Homing
```
G28 X         # Home X axis
G28           # Home all axis
M114          # Retrieve current position
```
If position is not accurate (x or y is not 0 after homing for eg:), check if there are any offsets set in firmware (define X_MIN_POS for Marlin for eg:).  
If z postition is off and there is no bed leveling set up, look at 'Z Probe Options' in Marlin and disable it.  

## Bed leveling
- Adjust the bed leveling screw such that it is not too tigh and not too losse. Make sure that the leveling screw is approzimately at the same level at all four corners
- Set the z end stop switch to be high enough for nozzle to not hit print bed
- Home using ` G28 ` (Nozzle will not be homed properly yet. But that is fine)
- Move Extruder to the middle of the print area by using ` G1 X100 Y100 F1500 `
- Get current position using ` M114 `
  - Pay attention to the z value. If it show 4 for example, that means printer thinks it is 4mm higher than 0
- Slowly move down z till it is about 1 mm away from the print bed using ` G1 Z[x mm] F1500 `
  -  Place a piece of paper in the middle of the print bed and continue brinnging the nozle down till there is slight friction when paper is pulled form under the nozle
- Shut down the steppers ` M18 `
- Move the extrider through x axis towards 0 and if the space between nozzle and bed is not consistant, turn z rod tread clockwisse or anti clockwise as appropriate to adjust the z height
- Once the space between nozzle and print bed is relatively consistant along x axis at middle of the print bed, it is time to do a bed leveling print and fine tune
 

### Extruder

```
M83         # Set E axes to relative mode
M104 S240   # Set nozle temperature to 240
M105        # Check curent temperature
G1 E50 F100 # Extrude 50mm 
```

Look for E value in M92 output from below command  
eg: M92 X80.00 Y80.00 Z400.00 E104.40
```
M503
```


Set E axis (Extruder) for relative positioning and extrude 50 mm after marking 70mm in filament   
```
M104 S240 
        ` M105 ` to check current temp
M83 
G1 E50 F100
```


Measure how much filament is left to the marking  

new e step = current e step * ( 50 / (70 - filament left))  
```
M92 E[new.val]  
M500
```

Repeat the process 2 or three times to get it right

### Fan

Fan on 
```
M106 S255
```
Fan off
```
M106 S0
```


### PID tuning

```
M503                # Report Settings . Look for M301 values for Hot end PID values
M303 E0 C8 S240     # Auto-tune hotend at 240 for 8 cycles
M301 Px.x Ix.x Dx.x # Set hotend PID . (once confirmed, better to add it to firmware)
M500                # Save settings
M303 E-1 C8 S60     # Auto-tune bed at 60 for 8 cycles
M304 Px.x Ix.x Dx.x # Set Bed PID
M500                # Save settings
```


### Misc commands

### Home
G28 Y X ; home x and y axes

### Set hot end temp
M104 S200

### Disable steppers
```
M18
```

### Current position 
```
M114
```

G90 ; use absolute positioning for the XYZ axes
G1 X20 F1500 ; move to X 20mm with feed rate 1500mm/min

G91 ; use relative positioning for the XYZ axes
G1 X5 F1500 ; move 10mm to the right of the current location and set feedrate to 1500mm/min



G90 
M106 S255 ; set the fan to full speed
M106 S127 ; set the fan to roughly 50% power
M106 S0 ; turn off the fan completely

M109 S190 T0 ; wait for T0 to reach 190 degrees before continuing with any other commands

G91 ; relative
G1 E2 F300 ;extrude 2mm at 300mm/min
G90 ;absolute movement 




